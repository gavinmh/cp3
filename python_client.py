import base64
import cv2
import requests


url = 'http://173.68.77.134:5001/v1/predictions'


def show_webcam(mirror=False):
    cam = cv2.VideoCapture(0)
    while True:
        ret_val, img = cam.read()
        if mirror:
            img = cv2.flip(img, 1)
        cv2.imshow('my webcam', img)

        s = base64.b64encode(img)
        r = base64.decodestring(s)
        files = {'media': r}
        # r = requests.post(url)
        r = requests.post(url, files=files)

        if cv2.waitKey(1) == 27:
            break  # esc to quit
    cv2.destroyAllWindows()


def main():
    show_webcam(mirror=True)

if __name__ == '__main__':
    main()
