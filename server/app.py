#!/usr/bin/env python
import datetime
import os
import random
from flask import Flask, jsonify, render_template, request
from util import cross_domain, resize_cover, resize_crop
import base64
import cStringIO
from PIL import Image
import numpy as np
import caffe


BASE_PATH = '/media/gavin/e2bcddc2-a2ea-4321-839e-29b2db8373dc/caffe'
DEFINITION = os.path.join(BASE_PATH, 'models/cp5/deploy.prototxt')
WEIGHTS = os.path.join(BASE_PATH, 'models/cp5/snapshots/cp5_iter_5000.caffemodel')
MEAN = '/media/gavin/e2bcddc2-a2ea-4321-839e-29b2db8373dc/caffe/data/ilsvrc12/imagenet_mean.binaryproto'
LABELS = os.path.join(BASE_PATH, 'data/cp5/labels.txt')
UPLOAD_FOLDER = '/media/gavin/e2bcddc2-a2ea-4321-839e-29b2db8373dc/cp3_app/server/img'

with open(LABELS) as handle:
    labels = [l.strip() for l in handle.readlines()]

blob = caffe.proto.caffe_pb2.BlobProto()
data = open(MEAN, 'rb').read()
blob.ParseFromString(data)
mean = np.array(caffe.io.blobproto_to_array(blob))[0].mean(1).mean(1)

caffe.set_mode_gpu()

app = Flask(__name__)


@app.route('/ping')
def ping():
    print('pong')
    return jsonify({'status': 'pong'})


@app.route('/')
def play():
    return render_template('index.html')


@app.route('/v1/predictions', methods=['POST', 'OPTIONS'])
@cross_domain(origin='*')
def predictions():
    if request.method == 'POST':
        image_string = cStringIO.StringIO(base64.b64decode(request.form['img']))
        image = Image.open(image_string)
        image = resize_crop(image, (256, 256))
        filename_ = str(datetime.datetime.now()).replace(' ', '_') + '.jpg'
        filename = os.path.join(UPLOAD_FOLDER, filename_)
        image.save(filename)

        img = caffe.io.load_image(filename)

        # arr = np.asarray(image)
        prediction = app.net.predict([img])

        label = None
        probability = None
        print(prediction)
        for i, idx in enumerate(prediction.flatten().argsort()[-1:][::-1], 1):
            print('Prediction %s: %s, %s' % (i, labels[idx], prediction.flatten()[idx]))
            label = labels[idx]
            probability = str(prediction.flatten()[idx])

        return jsonify({'label': label, 'probability': probability})


@app.route('/v1/foo')
def foo():
    pass


if __name__ == '__main__':
    app.net = caffe.Classifier(DEFINITION,
                               WEIGHTS,
                               mean=mean,
                               channel_swap=(2, 1, 0),
                               raw_scale=255,
                               image_dims=(256, 256))
    app.run(host='0.0.0.0', port=5001, debug=True, threaded=True)
