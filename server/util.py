import math
import urllib
import urllib2
from urlparse import urlparse
from datetime import timedelta
import requests
from functools import update_wrapper, wraps
from flask import make_response, request, current_app
import io
import hashlib
from cStringIO import StringIO
from PIL import Image, ImageChops


def resize_crop(image, size):
    """
    Crop the image with a centered rectangle of the specified size
    image:      a Pillow image instance
    size:       a list of two integers [width, height]
    """
    img_format = image.format
    image = image.copy()
    old_size = image.size
    left = max((old_size[0] - size[0]) / 2, 0)
    top = max((old_size[1] - size[1]) / 2, 0)
    right = min(old_size[0] - left, old_size[0])
    bottom = min(old_size[1] - top, old_size[1])
    rect = [int(math.ceil(x)) for x in (left, top, right, bottom)]
    left, top, right, bottom = rect
    crop = image.crop((left, top, right, bottom))
    if crop.size[0] < size[0] or crop.size[1] < size[1]:
        background = Image.new('RGB', (size[0], size[1]), (255, 255, 255, 0))
        img_position = (
            int(math.ceil((size[0] - crop.size[0]) / 2)),
            int(math.ceil((size[1] - crop.size[1]) / 2))
        )
        background.paste(crop, img_position)
        background.format = img_format
        return background
    else:
        crop.format = img_format
        return crop


def resize_cover(image, size):
    """
    Resize image according to size.
    image:      a Pillow image instance
    size:       a list of two integers [width, height]
    """
    img_format = image.format
    img = image.copy()
    img_size = img.size
    ratio = max(size[0] / img_size[0], size[1] / img_size[1])
    new_size = [
        int(math.ceil(img_size[0] * ratio)),
        int(math.ceil(img_size[1] * ratio))
    ]
    img = img.resize((new_size[0], new_size[1]), Image.ANTIALIAS)
    img = resize_crop(img, size)
    img.format = img_format
    return img


def cross_domain(origin=None, methods=None, headers=None, max_age=21600, attach_to_all=True, automatic_options=True):
    """Add Access Control Allow Origin headers to responses."""
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator
