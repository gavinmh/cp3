import requests
from PIL import Image
import base64


with open('/home/gavin/warrior-2.jpg', 'rb') as image_file:
    encoded_image = base64.b64encode(image_file.read())

url = 'http://173.68.77.134:5001/v1/predictions'
payload = {'img': encoded_image}

r = requests.post(url, data=payload)
r.json()